﻿using BLL.Interfaces;
using DTO;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IService<TaskDTO> _tasksService;

        public TasksController(IService<TaskDTO> tasksService)
        {
            _tasksService = tasksService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var tasks = await _tasksService.GetAllAsync();

            return Ok(tasks);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var task = await _tasksService.GetAsync(id);

            if (task == null) return NotFound(task);

            return Ok(task);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] TaskDTO task)
        {
            var created = await _tasksService.CreateAsync(task);

            if (!created) return BadRequest(task);

            return Created($"{task.Id}", task);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] TaskDTO task)
        {
            var updated = await _tasksService.UpdateAsync(task);

            if (!updated) return BadRequest(task);

            return Ok(task);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var deleted = await _tasksService.DeleteAsync(id);

            if (!deleted) return NotFound();

            return NoContent();
        }
    }
}
