﻿using BLL.Interfaces;
using DTO;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IService<ProjectDTO> _projecsService;

        public ProjectsController(IService<ProjectDTO> projecsService)
        {
            _projecsService = projecsService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var projects = await _projecsService.GetAllAsync();

            return Ok(projects);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var project = await _projecsService.GetAsync(id);

            if (project == null) return NotFound(project);

            return Ok(project);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] ProjectDTO project)
        {
            var created = await _projecsService.CreateAsync(project);

            if (!created) return BadRequest(project);

            return Created($"{project.Id}", project);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] ProjectDTO project)
        {
            var updated = await _projecsService.UpdateAsync(project);

            if (!updated) return BadRequest(project);

            return Ok(project);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var deleted = await _projecsService.DeleteAsync(id);

            if (!deleted) return NotFound();

            return NoContent();
        }
    }
}
