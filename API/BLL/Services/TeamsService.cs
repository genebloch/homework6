﻿using BLL.Interfaces;
using DAL.Interfaces;
using DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TeamsService : IService<TeamDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AutoMapper.IMapper _mapper;

        public TeamsService(IUnitOfWork unitOfWork, AutoMapper.IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> CreateAsync(TeamDTO item)
        {
            var mapped = _mapper.Map<DAL.Models.Team>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;

            await _unitOfWork.Teams.CreateAsync(mapped);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var status = await _unitOfWork.Teams.DeleteAsync(id);

            await _unitOfWork.SaveChangesAsync();

            return status;
        }

        public async Task<TeamDTO> GetAsync(int id)
        {
            var team = await _unitOfWork.Teams.GetAsync(id);

            return _mapper.Map<TeamDTO>(team);
        }

        public async Task<IEnumerable<TeamDTO>> GetAllAsync()
        {
            var teams = await _unitOfWork.Teams.GetAllAsync();

            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }

        public async Task<bool> UpdateAsync(TeamDTO item)
        {
            var mapped = _mapper.Map<DAL.Models.Team>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;

            await _unitOfWork.Teams.UpdateAsync(mapped);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }
    }
}
