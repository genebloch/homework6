﻿using AutoMapper;
using BLL.AdditionalStructures;
using DAL.Interfaces;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class LinqService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public LinqService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IDictionary<ProjectDTO, int>> GetUserTasksInProjectAsync(int userId)
        {
            var projects = await _unitOfWork.Projects.GetAllAsync();
            var tasks = await _unitOfWork.Tasks.GetAllAsync();

            return projects.Where(p => p.AuthorId == userId).Select(p => new
            {
                project = p,
                tasksAmount = tasks.Count(task => task.ProjectId == p.Id)
            }).ToDictionary(x => _mapper.Map<ProjectDTO>(x.project), x => x.tasksAmount);
        }

        public async Task<IEnumerable<TaskDTO>> GetUserTasksWithRestrictedNameLengthAsync(int userId)
        {
            var tasks = await _unitOfWork.Tasks.GetAllAsync();

            var result = tasks.Where(t => t.PerformerId == userId)
                .Where(t => t.Name.Length < 45).ToList();

            return _mapper.Map<IEnumerable<TaskDTO>>(result);
        }

        public async Task<IEnumerable<TaskName>> GetUserFinishedTasksIn2020Async(int userId)
        {
            var tasks = await _unitOfWork.Tasks.GetAllAsync();

            return tasks.Where(t => t.PerformerId == userId)
                .Where(t => t.FinishedAt.Year == 2020)
                .Select(t => new TaskName() { Id = t.Id, Name = t.Name }).ToList();
        }

        public async Task<IEnumerable<UserDTO>> GetUsersOlderThan10YearsAsync()
        {
            var users = await _unitOfWork.Users.GetAllAsync();

            var result = users.Where(u => DateTime.Now.Year - u.Birthday.Year > 10)
                .OrderByDescending(u => u.RegisteredAt)
                .GroupBy(u => u.TeamId)
                .SelectMany(u => u).ToList();

            return _mapper.Map<IEnumerable<UserDTO>>(result);
        }

        public async Task<IEnumerable<UserWithTasks>> GetSortedUsersWithTasksAsync()
        {
            var users = await _unitOfWork.Users.GetAllAsync();
            var tasks = await _unitOfWork.Tasks.GetAllAsync();

            var output = new List<UserWithTasks>();

            var query = users.OrderBy(u => u.FirstName).Select(u => new
            {
                user = u,
                tasks = tasks.Where(t => t.PerformerId == u.Id).OrderByDescending(t => t.Name.Length).ToList()
            }).ToList();

            foreach (var obj in query)
            {
                output.Add(new UserWithTasks() { User = _mapper.Map<UserDTO>(obj.user), Tasks = _mapper.Map<IEnumerable<TaskDTO>>(obj.tasks) });
            }

            return output;
        }

        public async Task<UserInfo> GetUserInfoAsync(int userId)
        {
            var projects = await _unitOfWork.Projects.GetAllAsync();
            var tasks = await _unitOfWork.Tasks.GetAllAsync();

            var query = new
            {
                last = projects.Where(project => project.AuthorId == userId).OrderByDescending(p => p.CreatedAt).FirstOrDefault(),
                tasksAmount = -1, //Загальна кількість тасків під останнім проектом
                notFinishedTasks = tasks.Where(t => t.PerformerId == userId).Count(t => t.State == 3 || t.State == 4),
                theLongest = tasks.Where(t => t.PerformerId == userId).OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault()
            };

            return new UserInfo()
            {
                LastProject = _mapper.Map<ProjectDTO>(query.last),
                NotFinishedTasks = query.notFinishedTasks,
                TasksAmount = query.tasksAmount,
                TheLongest = _mapper.Map<TaskDTO>(query.theLongest)
            };
        }

        public async Task<IEnumerable<ProjectInfo>> GetProjectsInfoAsync()
        {
            var projects = await _unitOfWork.Projects.GetAllAsync();
            var tasks = await _unitOfWork.Tasks.GetAllAsync();

            var output = new List<ProjectInfo>();

            var query = projects.Select(p => new
            {
                project = p,
                theLongestDescription = tasks.Where(t => t.ProjectId == p.Id).OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                theShortestName = tasks.Where(t => t.ProjectId == p.Id).OrderBy(t => t.Name.Length).FirstOrDefault(),
                usersAmount = -1 //Загальна кількість користувачів в команді проекту, де або опис проекту >20 символів, або кількість тасків <3
            }).ToList();

            foreach (var obj in query)
            {
                output.Add(
                    new ProjectInfo() 
                    { 
                        Project = _mapper.Map<ProjectDTO>(obj.project), 
                        TheLongestDescription = _mapper.Map<TaskDTO>(obj.theLongestDescription),
                        TheShortestName = _mapper.Map<TaskDTO>(obj.theShortestName),
                        UsersAmount = obj.usersAmount
                    });
            }

            return output;
        }

        public async Task<IEnumerable<TaskDTO>> GetUserNotFinishedTasksAsync(int id)
        {
            var tasks = await _unitOfWork.Tasks.GetAllAsync();

            var result = tasks.Where(t => t.PerformerId == id && t.State != 3).ToList();

            return _mapper.Map<IEnumerable<TaskDTO>>(result);
        }
    }
}
