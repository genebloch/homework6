﻿using DTO;

namespace BLL.AdditionalStructures
{
    public class ProjectInfo
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO TheLongestDescription { get; set; }
        public TaskDTO TheShortestName { get; set; }
        public int UsersAmount { get; set; }
    }
}
