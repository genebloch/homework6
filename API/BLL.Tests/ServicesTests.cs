using AutoMapper;
using BLL.Interfaces;
using BLL.Services;
using DAL.Interfaces;
using DAL.Models;
using DTO;
using FakeItEasy;
using Xunit;
using BLL.Helpers;

namespace BLL.Tests
{
    public class ServicesTests
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        private readonly IService<UserDTO> _usersService;
        private readonly IService<TaskDTO> _tasksService;

        public ServicesTests()
        {
            var mappingConfig = new MapperConfiguration(mc => mc.AddProfile(new MapperProfile()));
            _mapper = mappingConfig.CreateMapper();

            _unitOfWork = A.Fake<IUnitOfWork>();

            _usersService = new UsersService(_unitOfWork, _mapper);
            _tasksService = new TasksService(_unitOfWork, _mapper);
        }

        [Theory]
        [InlineData("test1@gmail.com")]
        [InlineData("test2@gmail.com")]
        [InlineData("test3@gmail.com")]
        public void CreateUser_WhenModelIsCorrect_ThanCreateMustHaveHappened(string email)
        { 
            //arrange
            var user = new UserDTO() { Email = email };

            //act
            _usersService.CreateAsync(user);

            //assert
            A.CallTo(() => _unitOfWork.Users.CreateAsync(A<User>.That.Matches(u => u.Email == user.Email))).MustHaveHappened();
        }

        [Fact]
        public void CreateUser_WhenModelIsInvalid_ThanCreateMustNotHaveHappened()
        {
            //arrange
            var user = new UserDTO() { Id = 1 };

            //act
            _usersService.CreateAsync(user);

            //assert
            A.CallTo(() => _unitOfWork.Users.CreateAsync(A<User>.That.Matches(u => u.Id == user.Id))).MustNotHaveHappened();
        }

        [Fact]
        public void MarkTaskAsFinished_WhenModelIsCorrect_ThanUpdateMustHaveHappened()
        {
            var task = new TaskDTO() { Id = 1, State = 3, PerformerId = 5, ProjectId = 2, Name = "name" };

            _tasksService.UpdateAsync(task);

            A.CallTo(() => _unitOfWork.Tasks.UpdateAsync(A<Task>.That.Matches(t => t.Id == task.Id))).MustHaveHappened();
        }

        [Fact]
        public void MarkTaskAsFinished_WhenModelIsInvalid_ThanUpdateMustNotHaveHappened()
        {
            var task = new TaskDTO() { Id = 1, State = 3 };

            _tasksService.UpdateAsync(task);

            A.CallTo(() => _unitOfWork.Tasks.UpdateAsync(A<Task>.That.Matches(t => t.Id == task.Id))).MustNotHaveHappened();
        }

        [Fact]
        public void AddUserToTeam_ThanUpdateMustHappened()
        {
            var user = new UserDTO() { Id = 1, Email = "test@gmail.com", TeamId = 5 };

            _usersService.UpdateAsync(user);

            A.CallTo(() => _unitOfWork.Users.UpdateAsync(A<User>.That.Matches(u => u.Id == user.Id))).MustHaveHappened();
        }

        [Fact]
        public void AddUserToTeam_ThanCreateMustHappened()
        {
            var user = new UserDTO() { Id = 1, Email = "test@gmail.com", TeamId = 4 };

            _usersService.CreateAsync(user);

            A.CallTo(() => _unitOfWork.Users.CreateAsync(A<User>.That.Matches(u => u.Id == user.Id))).MustHaveHappened();
        }

    }
}
