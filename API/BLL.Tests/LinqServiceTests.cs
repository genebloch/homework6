﻿using AutoMapper;
using BLL.Services;
using DAL.Interfaces;
using DAL.Models;
using FakeItEasy;
using Xunit;
using BLL.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Tests
{
    public class LinqServiceTests
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        private readonly LinqService _linqService;

        public LinqServiceTests()
        {
            var mappingConfig = new MapperConfiguration(mc => mc.AddProfile(new MapperProfile()));
            _mapper = mappingConfig.CreateMapper();

            _unitOfWork = A.Fake<IUnitOfWork>();

            _linqService = new LinqService(_unitOfWork, _mapper);
        }

        [Fact]
        public void GetUserTasksInProject_ThanCorrectDictionaryMustBeReturned()
        {
            A.CallTo(() => _unitOfWork.Projects.GetAllAsync()).Returns(new List<Project>()
            {
                new Project() { Id = 1, Name = "Project1", AuthorId = 1 },
                new Project() { Id = 2, Name = "Project2", AuthorId = 5 },
                new Project() { Id = 3, Name = "Project3", AuthorId = 3 },
                new Project() { Id = 4, Name = "Project4", AuthorId = 5 },
                new Project() { Id = 5, Name = "Project5", AuthorId = 5 }
            });

            A.CallTo(() => _unitOfWork.Tasks.GetAllAsync()).Returns(new List<Task>()
            {
                new Task() { Id = 1, Name = "Task1", ProjectId = 5 },
                new Task() { Id = 2, Name = "Task2", ProjectId = 2 },
                new Task() { Id = 3, Name = "Task3", ProjectId = 2 },
                new Task() { Id = 4, Name = "Task4", ProjectId = 1 },
                new Task() { Id = 5, Name = "Task5", ProjectId = 3 }
            });

            var actualResult = _linqService.GetUserTasksInProjectAsync(5).Result;

            Assert.Equal(3, actualResult.Count);
            Assert.Equal(2, actualResult.ElementAt(0).Value);
            Assert.Equal(0, actualResult.ElementAt(1).Value);
        }

        [Fact]
        public void GetUserTasksWithRestrictedNameLength_ThanListLengthMustBeEqualTo2()
        {
            A.CallTo(() => _unitOfWork.Tasks.GetAllAsync()).Returns(new List<Task>()
            {
                new Task() { Id = 1, Name = "Project1", PerformerId = 1 },
                new Task() { Id = 2, Name = "Project2 Project2 Project2 Project2 Project2 Project2", PerformerId = 5 },
                new Task() { Id = 3, Name = "Project3", PerformerId = 3 },
                new Task() { Id = 4, Name = "Project4", PerformerId = 5 },
                new Task() { Id = 5, Name = "Project5", PerformerId = 5 }
            });

            var actualResult = _linqService.GetUserTasksWithRestrictedNameLengthAsync(5).Result;

            Assert.Equal(2, actualResult.Count());
        }

        [Fact]
        public void GetUserFinishedTasksIn2020_ThanListLengthMustBeEqualTo2()
        {
            A.CallTo(() => _unitOfWork.Tasks.GetAllAsync()).Returns(new List<Task>()
            {
                new Task() { Id = 1, FinishedAt = new System.DateTime(2020, 5, 30), PerformerId = 1 },
                new Task() { Id = 2, FinishedAt = new System.DateTime(2015, 2, 25), PerformerId = 5 },
                new Task() { Id = 3, FinishedAt = new System.DateTime(2018, 4, 11), PerformerId = 3 },
                new Task() { Id = 4, FinishedAt = new System.DateTime(2020, 6, 1), PerformerId = 5 },
                new Task() { Id = 5, FinishedAt = new System.DateTime(2020, 7, 20), PerformerId = 5 }
            });

            var actulaResult = _linqService.GetUserFinishedTasksIn2020Async(5).Result;

            Assert.Equal(2, actulaResult.Count());
        }

        [Fact]
        public void GetUsersOlderThan10Years_ThanCorrectListMustBeReturned()
        {
            A.CallTo(() => _unitOfWork.Users.GetAllAsync()).Returns(new List<User>()
            {
                new User() 
                { 
                    Id = 1, 
                    Birthday = new System.DateTime(2000, 8, 25), 
                    RegisteredAt = new System.DateTime(2018, 5, 7),
                    TeamId = 1
                },
                new User()
                { 
                    Id = 2, 
                    Birthday = new System.DateTime(2015, 1, 2),
                    RegisteredAt = new System.DateTime(2017, 2, 1),
                    TeamId = 2
                },
                new User()
                { 
                    Id = 3, 
                    Birthday = new System.DateTime(1998, 5, 5),
                    RegisteredAt = new System.DateTime(2019, 2, 25),
                    TeamId = 1
                },
                new User()
                {
                    Id = 4,
                    Birthday = new System.DateTime(2005, 3, 7),
                    RegisteredAt = new System.DateTime(2015, 1, 1),
                    TeamId = 3
                },
                new User() 
                { 
                    Id = 5, 
                    Birthday = new System.DateTime(2018, 5, 1),
                    RegisteredAt = new System.DateTime(2019, 1, 1),
                    TeamId = 2
                }
            });

            var actulaResult = _linqService.GetUsersOlderThan10YearsAsync().Result;

            Assert.Equal(3, actulaResult.Count());
            Assert.True(actulaResult.ElementAt(0).Id == 3);
            Assert.True(actulaResult.ElementAt(1).Id == 1);
        }

        [Fact]
        public void GetSortedUsersWithTasks_ThanCorrectListMustBeReturned()
        {
            A.CallTo(() => _unitOfWork.Users.GetAllAsync()).Returns(new List<User>()
            {
                new User() { Id = 1, FirstName = "Brian" },
                new User() { Id = 2, FirstName = "Elon" },
                new User() { Id = 3, FirstName = "Eugene" },
                new User() { Id = 4, FirstName = "Mark" },
                new User() { Id = 5, FirstName = "Andriy" }
            });

            A.CallTo(() => _unitOfWork.Tasks.GetAllAsync()).Returns(new List<Task>()
            {
                new Task() { Id = 1, PerformerId = 2, Name = "Task name" },
                new Task() { Id = 2, PerformerId = 2, Name = "Task name Task name Task name" },
                new Task() { Id = 3, PerformerId = 3, Name = "Task name Task name" },
                new Task() { Id = 4, PerformerId = 5, Name = "Task name" },
                new Task() { Id = 5, PerformerId = 5, Name = "Task name Task name Task name Task name" }
            });

            var actulaResult = _linqService.GetSortedUsersWithTasksAsync().Result;

            Assert.Equal(5, actulaResult.Count());
            Assert.True(actulaResult.ElementAt(0).User.Id == 5);
            Assert.True(actulaResult.ElementAt(0).Tasks.ElementAt(0).Name == "Task name Task name Task name Task name");
            Assert.True(actulaResult.ElementAt(1).User.Id == 1);
            Assert.True(actulaResult.ElementAt(1).Tasks.Count() == 0);
        }

        [Fact]
        public void GetUserInfo_ThanCorrectDataMustBeReturned()
        {
            A.CallTo(() => _unitOfWork.Projects.GetAllAsync()).Returns(new List<Project>()
            {
                new Project() { Id = 1, AuthorId = 5, CreatedAt = new System.DateTime(2015, 5, 2) },
                new Project() { Id = 2, AuthorId = 2, CreatedAt = new System.DateTime(2018, 2, 1) },
                new Project() { Id = 3, AuthorId = 3, CreatedAt = new System.DateTime(2014, 1, 8) },
                new Project() { Id = 4, AuthorId = 5, CreatedAt = new System.DateTime(2016, 8, 25) },
                new Project() { Id = 5, AuthorId = 5, CreatedAt = new System.DateTime(2019, 4, 7) }
            });

            A.CallTo(() => _unitOfWork.Tasks.GetAllAsync()).Returns(new List<Task>()
            {
                new Task()
                { 
                    Id = 1, 
                    PerformerId = 1, 
                    CreatedAt = new System.DateTime(2015, 5, 2),
                    FinishedAt = new System.DateTime(2015, 8, 1),
                    State = 1
                },
                new Task()
                { 
                    Id = 2, 
                    PerformerId = 5,
                    CreatedAt = new System.DateTime(2018, 2, 1),
                    FinishedAt = new System.DateTime(2018, 8, 4),
                    State = 2
                },
                new Task()
                { 
                    Id = 3,
                    PerformerId = 5, 
                    CreatedAt = new System.DateTime(2014, 1, 8),
                    FinishedAt = new System.DateTime(2015, 4, 3),
                    State = 3
                },
                new Task()
                {
                    Id = 4,
                    PerformerId = 3, 
                    CreatedAt = new System.DateTime(2016, 8, 25),
                    FinishedAt = new System.DateTime(2016, 9, 1),
                    State = 3
                },
                new Task()
                { 
                    Id = 5, 
                    PerformerId = 5, 
                    CreatedAt = new System.DateTime(2019, 4, 7),
                    FinishedAt = new System.DateTime(2020, 2, 3),
                    State = 4
                }
            });

            var actualResult = _linqService.GetUserInfoAsync(5).Result;

            Assert.Equal(2, actualResult.NotFinishedTasks);
            Assert.True(actualResult.TheLongest.Id == 3);
            Assert.True(actualResult.LastProject.Id == 5);
        }

        [Fact]
        public void GetProjectsInfo_ThanCorrectListMustBeReturned()
        {
            A.CallTo(() => _unitOfWork.Projects.GetAllAsync()).Returns(new List<Project>()
            {
                new Project() { Id = 1, Name = "Project1" },
                new Project() { Id = 2, Name = "Project2" },
                new Project() { Id = 3, Name = "Project3" },
                new Project() { Id = 4, Name = "Project4" },
                new Project() { Id = 5, Name = "Project5" }
            });

            A.CallTo(() => _unitOfWork.Tasks.GetAllAsync()).Returns(new List<Task>()
            {
                new Task() 
                { 
                    Id = 1, 
                    Name = "Task1 Task1",
                    ProjectId = 5,
                    Description = "Description1 Description1"
                },
                new Task()
                { 
                    Id = 2,
                    Name = "Task2",
                    ProjectId = 5,
                    Description = "Description 2"
                },
                new Task() 
                { 
                    Id = 3, 
                    Name = "Task3 Task3 Task 3",
                    ProjectId = 2,
                    Description = "Description"
                },
                new Task()
                { 
                    Id = 4, 
                    Name = "Task4 Task4 Task4",
                    ProjectId = 5,
                    Description = "Description4 Description4"
                },
                new Task() 
                { 
                    Id = 5,
                    Name = "Task",
                    ProjectId = 5,
                    Description = "Description5"
                }
            });

            var actualResult = _linqService.GetProjectsInfoAsync().Result;

            Assert.Equal(5, actualResult.Count());
            Assert.True(actualResult.ElementAt(4).TheShortestName.Id == 5);
            Assert.True(actualResult.ElementAt(4).TheLongestDescription.Id == 1);
            Assert.True(actualResult.ElementAt(0).TheShortestName == null);
            Assert.True(actualResult.ElementAt(0).TheLongestDescription == null);
        }
    }
}
